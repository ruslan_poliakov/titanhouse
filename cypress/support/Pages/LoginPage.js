//LOCATORS
const emailField = '#adornment-email'
const passwordField = '#adornment-password'
const signIn = 'button[type=submit]'
const linkedId = 'a[href*=linkedin]'

//enter email, password and click login
Cypress.Commands.add('Login', (email, password) => {
  return cy
    .log(`Login with user: ${email} , password: ${password}`)
    .get(emailField).clear()
    .get(emailField).type(email)
    .get(passwordField).clear()
    .get(passwordField).type(password)
    .get(signIn).click()
});
//Click LinkedIn sign up
Cypress.Commands.add('LinkedIn', () => {
  return cy
    .get(linkedId).click()
});
