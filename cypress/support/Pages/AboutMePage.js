//LOCATORS
const edit ='a.edit-link'
const addressEditField = 'input[role=combobox]'
const save = 'div._buttons>button.btn-secondary'
const addressLabel = 'p.text-center'

//Click edit
Cypress.Commands.add('EditAboutMe', () => {
    return cy
    .get(edit).click()
});

//edit address
Cypress.Commands.add('AddressEnter', (text) => {
    return cy
    .get(addressEditField).first().click({ force: true })
    .get(addressEditField).first().clear()
    .get(addressEditField).first().type(text)
});

//Click Save
Cypress.Commands.add('SaveAboutMe', () => {
    return cy
    .get(save).click()
});

//Click Save
Cypress.Commands.add('AssertAddressIs', (address) => {
    return cy
    .get(addressLabel).contains(address)
});

