//LOCATORS
const preferenceSwitch ='input[class*=MuiSwitchBase-input]'
const preferenceText = 'span[class*=MuiSwitch-root]'
const profileTab ='li.nav-item>div.dropdown:contains("Profile")'
const aboutMeMenu = 'button.dropdown-item:contains("About me")'

//check dashboard URL
Cypress.Commands.add('AssertDashboardURL', () => {
  return cy
  .url().should('include', '/profilePercentage')
});

//set preference to Public or Privated
Cypress.Commands.add('SetPreference', (text) => {
  if (text=='Public')
    cy.get(preferenceSwitch).check();
  else
    cy.get(preferenceSwitch).uncheck(); 
  return cy
});

//Assert preference label text
Cypress.Commands.add('AssertPreferenceIs', (expected) => {
  return cy
  .get(preferenceText).parent().contains(expected) 
});

//Open Profile > About me
Cypress.Commands.add('GoToAboutMe', () => {
  return cy
  .get(profileTab).click()
  .get(aboutMeMenu).click()
});