describe('Tests examples', function() {
  
    it('Login the Titan House', function() {
      cy.visit('/')
      .Login('marbra8764@gmail.com', 'Ww!12222')
      .AssertDashboardURL()
    })

    it('Visible preference switch', function() {
        cy.SetPreference('Private')
        .AssertPreferenceIs('Private')
    }) 

    it('Edit About Me', function() {
      cy.GoToAboutMe()
      .EditAboutMe()
      .AddressEnter('72 Faxcol Dr')
      .SaveAboutMe()
      .AssertAddressIs('72 Faxcol Dr')
    }) 
  })